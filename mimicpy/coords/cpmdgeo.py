#
#    MiMiCPy: Python Based Tools for MiMiC
#    Copyright (C) 2020-2023 Bharath Raghavan,
#                            Florian Schackert
#
#    This file is part of MiMiCPy.
#
#    MiMiCPy is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Lesser General Public License as
#    published by the Free Software Foundation, either version 3 of
#    the License, or (at your option) any later version.
#
#    MiMiCPy is distributed in the hope that it will be useful, but
#    WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#    GNU Lesser General Public License for more details.
#
#    You should have received a copy of the GNU Lesser General Public License
#    along with this program. If not, see <http://www.gnu.org/licenses/>.
#

"""Module for CPMD GEOMETRY files"""

import numpy as np
import pandas as pd
from ..utils.errors import MiMiCPyError, ParserError
from ..utils.constants import BOHR_RADIUS
from .base import BaseCoordsClass

class CPMDGeo(BaseCoordsClass):
    """reads and writes CPMD GEOMETRY files"""
    
    def __init__(self, file_name, buffer=1000, cpmd=None, top=None):
        self.file_name = file_name
        self.buffer = buffer
        if not cpmd or not top:
            raise MiMiCPyError("A CPMD input and GROMACS Topology object are required to read CPMD GEOMETRY files.")
        else:
            id_conversion_dict = cpmd.gmx_to_cpmd_idx(top)
            self.coords_index = id_conversion_dict.keys()
        self._number_of_cols = 6
            
    def _read(self):
        """Read atom coordinates"""
        
        values, cols = self._read_as_np()
        
        if cols != self._number_of_cols:
            raise ParserError(self.file_name, 'CPMD Geometry', details='Some columns are missing.')
        
        values *= BOHR_RADIUS
        
        number_of_atoms = len(values)/self._number_of_cols
        
        if number_of_atoms.is_integer():
            number_of_atoms = int(number_of_atoms)
        else:
            raise ParserError(self.file_name, 'CPMD Geometry', details='Lines in file are missing.')

        return self._get_coords(values, number_of_atoms), None
    
    def _get_coords(self, values, number_of_atoms):
        coords = pd.DataFrame(values.reshape(number_of_atoms, self._number_of_cols), columns=['x', 'y', 'z', 'v_x', 'v_y', 'v_z'])
        coords['id'] = self.coords_index
        return coords.set_index(['id']).sort_index()
    
    def _write(self, mpt_coords, box, title):
        pass

class CPMDGeoXYZ(CPMDGeo):
    """reads and writes CPMD GEOMETRY.xyz files"""

    def _read(self):
        """Read atom coordinates"""
        
        number_of_atoms, cols = self.file.readline()
        
        if cols != self._number_of_cols+1:
            raise ParserError(self.file_name, 'CPMD Geometry', details='Some columns are missing.')
        
        if number_of_atoms.strip().isdigit():
            number_of_atoms = int(number_of_atoms)
        else:
            raise ParserError(self.file_name, 'CPMD Geometry XYZ', details='First line should be an integer.')

        self.file.readline() # comment line
        values = self._read_as_np()
        
        values /= 10
        
        number_of_atoms_2 = len(values)/self._number_of_cols
        
        if number_of_atoms > number_of_atoms_2:
            raise ParserError(self.file_name, 'CPMD Geometry XYZ', details='Number of atoms greater than number of entries.')
        elif number_of_atoms < number_of_atoms_2:
            raise ParserError(self.file_name, 'CPMD Geometry XYZ', details='Number of atoms less than number of entries.')
            
        return self._get_coords(values, number_of_atoms), None
    
    def _write(self, mpt_coords, box, title):
        pass