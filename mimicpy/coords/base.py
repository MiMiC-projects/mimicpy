#
#    MiMiCPy: Python Based Tools for MiMiC
#    Copyright (C) 2020-2023 Bharath Raghavan,
#                            Florian Schackert
#
#    This file is part of MiMiCPy.
#
#    MiMiCPy is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Lesser General Public License as
#    published by the Free Software Foundation, either version 3 of
#    the License, or (at your option) any later version.
#
#    MiMiCPy is distributed in the hope that it will be useful, but
#    WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#    GNU Lesser General Public License for more details.
#
#    You should have received a copy of the GNU Lesser General Public License
#    along with this program. If not, see <http://www.gnu.org/licenses/>.
#

from abc import ABC, abstractmethod
import numpy as np
from ..topology.mpt import Mpt
from ..utils.errors import MiMiCPyError, ParserError
from ..utils.file_handler import Parser, write as write_string

class BaseCoordsClass(ABC):

    def __init__(self, file_name, buffer=1000):
        self.file_name = file_name
        self.buffer = buffer

    def read(self):
        self.file = Parser(self.file_name, self.buffer)
        coords, box = self._read()
        self.file.close()
        
        if box is None:
            box = [0.0, 0.0, 0.0]
            for i, r in enumerate(['x', 'y', 'z']):
                box[i] = abs(max(coords[r]) - min(coords[r]))
        
        return coords, box

    @abstractmethod
    def _read(self):
        pass

    def _read_as_np(self):

        def mapped(value):
            if value.isnumeric():
                return np.nan
            try:
                return float(value)
            except ValueError:
                return np.nan

        def string_to_array(string):
            return np.array(list(map(mapped, string.split())))
        
        first_atom_line = self.file.readline()
        values = string_to_array(first_atom_line)
        self.file.buffer *= len(first_atom_line)
        for string in self.file:
            values = np.append(values, string_to_array(string))
        number_of_cols = len(first_atom_line.split())
        return values[~np.isnan(values)], number_of_cols
    
    def write(self, sele, coords=None, box=None, as_str=False, title=''):
        if isinstance(sele, Mpt):
            sele = sele.select('all')
        if coords is not None:
            sele = sele.merge(coords, left_on='id', right_on='id')
        s = self._write(sele.reset_index(), box, title)
        if as_str:
            return s
        write_string(s, self.file_name, 'w')

    @abstractmethod
    def _write(self, mpt_coords, box, title):
        pass

    def str_checker(self, s, n):
        if len(s) > n:
            s = s[:n]
        return s

    def int_checker(self, i, n):
        return int(self.str_checker(str(i), n))


class CoordsIO:

    def __init__(self, file_name, mode='r', ext=None, **kwargs):
        if isinstance(file_name, BaseCoordsClass):
            self.__coords_obj = file_name
        else:
            if ext is None:
                ext = file_name.split('.')[-1]

            if ext == 'gro':
                from .gro import Gro
                self.__coords_obj = Gro(file_name, **kwargs)
            elif ext == 'pdb':
                from .pdb import Pdb
                self.__coords_obj = Pdb(file_name, **kwargs)
            elif ext == 'cpmd geo':
                from .cpmdgeo import CPMDGeo
                self.__coords_obj = CPMDGeo(file_name, **kwargs)
            elif ext == 'cpmd geo xyz' or ext == 'xyz':
                from .cpmdgeo import CPMDGeoXYZ
                self.__coords_obj = CPMDGeoXYZ(file_name, **kwargs)
            else:
                raise ParserError('Unknown coordinate format')

        self.mode = mode
        self._coords = None
        self._box = None

        if mode == 'r':
            self.__read()
        elif mode == 'w':
            pass
        else:
            raise MiMiCPyError('{} is not a mode. Only r or w can be used'.format(mode))

    @property
    def coords(self):
        if self.mode != 'r':
            self.__read()
        return self._coords

    @property
    def box(self):
        if self.mode != 'r':
            self.__read()
        return self._box

    def __read(self):
        self.mode = 'r'
        self._coords, self._box = self.__coords_obj.read()

    def write(self, sele, coords=None, as_str=False, box=None, title=''):
        if self.mode != 'w':
            self.mode = 'w'
        return self.__coords_obj.write(sele, coords, box, as_str, title)

    def __enter__(self):
        return self

    def __exit__(self, type, value, traceback):
        pass
