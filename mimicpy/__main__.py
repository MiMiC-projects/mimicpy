#!/usr/bin/env python

#
#    MiMiCPy: Python Based Tools for MiMiC
#    Copyright (C) 2020-2023 Bharath Raghavan,
#                            Florian Schackert
#
#    This file is part of MiMiCPy.
#
#    MiMiCPy is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Lesser General Public License as
#    published by the Free Software Foundation, either version 3 of
#    the License, or (at your option) any later version.
#
#    MiMiCPy is distributed in the hope that it will be useful, but
#    WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#    GNU Lesser General Public License for more details.
#
#    You should have received a copy of the GNU Lesser General Public License
#    along with this program. If not, see <http://www.gnu.org/licenses/>.
#

import sys
from os.path import isfile
import time
import argparse
import readline
import warnings
import itertools
import threading
import pandas as pd
import mimicpy

warnings.simplefilter(action='ignore', category=FutureWarning)  # Supress pandas warnings


class Loader:

    def __init__(self, message):

        self.done = False
        self.message = message
        t = threading.Thread(target=self.__animate)
        t.start()

    def __animate(self):
        for c in itertools.cycle(['|', '/', '-', '\\']):
            if self.done:
                break
            sys.stdout.write('\r{}  '.format(self.message) + c)
            sys.stdout.flush()
            time.sleep(0.1)

    def close(self, halt=False):
        self.done = True
        if not halt:
            print('Done')

# TODO: Unify the three helper functions
def __str2bool(string):
    if isinstance(string, bool):
        return string
    if string.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    if string.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        print('Error! boolean value expected for argument\n')
        sys.exit(1)

def __str2float(v):
    if v is not None:
        try:
            return float(v)
        except:
            print('Error! float value expected for argument\n')
            sys.exit(1)
    else:
        return 0.0

def __str2int(v):
    try:
        return int(v)
    except:
        print('Error! integer value expected for argument\n')
        sys.exit(1)

def prepqm(args):

    def selection_help():
        print("\nvalid subcommands:\n\n"
              "    o add       <selection>    add selected atoms to QM region\n"
              "    o add-bound  <selection>    add selected atoms to QM region as boundary atoms\n"
              "    o delete    <selection>    delete selected atoms from QM region\n"
              "    o clear                    clear all atoms from QM region\n"
              "    o view      <file-name>    print current QM region to console or file\n"
              "    o quit                     create CPMD input and GROMACS index files and quit\n"
              "    o help                     show this help message\n\n"
              "For more information on the selection language please refer to the docs.\n")

    def view(file_name=None):
        if prep.qm_atoms.empty:
            print("No QM atoms have been selected")
        else:
            # display whole dataframe
            with pd.option_context('display.max_rows', None, 'display.max_columns', None):
                qmatoms_str = str(prep.qm_atoms)

            if file_name:
                mimicpy.utils.file_handler.write(qmatoms_str, file_name)
                print("Wrote list of QM atoms to {}".format(file_name))
            else:
                print(qmatoms_str)

    mpt = get_nsa_mpt(args)

    print('')
    loader = Loader('**Reading coordinates**')

    try:
        selector = mimicpy.DefaultSelector(mpt, args.coords, buffer=args.bufc)
        prep = mimicpy.Preparation(selector)
    except FileNotFoundError as e:
        print('\n\nError: Cannot find file {}! Exiting..\n'.format(e.filename))
        loader.close(halt=True)
        sys.exit(1)
    except (mimicpy.utils.errors.ParserError, mimicpy.utils.errors.MiMiCPyError) as e:
        print(e)
        loader.close(halt=True)
        sys.exit(1)
    except KeyboardInterrupt:
        print("Reading halted\n")
        loader.close(halt=True)
        sys.exit(1)

    loader.close()

    readline.parse_and_bind('set editing-mode vi') # handle command history

    dispatch = {'add':prep.add,
                'add-bound': lambda selection: prep.add(selection, True),
                'delete':prep.delete,
                'clear': prep.clear,
                'view':view,
                'help':selection_help}

    def check_user_input(user_input):
        user_input = user_input.split()
        try:
            command = user_input[0].lower()
        except IndexError: # handle empty commands
            return False

        selection = ' '.join(user_input[1:])
        try:
            dispatch[command](selection)
        except mimicpy.utils.errors.SelectionError as e:  # Selection errors
            print(e)
        except TypeError:  # include functions without argument
            dispatch[command]()
        except KeyError: # invalid commands
            print("{} is an invalid command! Please try again. Type 'help' for more information.".format(command))

    def do_prep():
        try:
            if not prep.qm_atoms.empty:
                if args.bound:
                    prep.find_bound_atoms()

                prep.get_mimic_input(args.inp, args.ndx, args.out,
                                     args.pad, args.abs, args.qma,
                                     args.path, args.q, args.pp)

                if args.mdp:
                    print("Checking that {} is consistent with a MiMiC run".format(args.mdp))
                    new_mdp = "fixed_{}".format(args.mdp)
                    prepmm(mdp=args.mdp, qma=args.qma, out=new_mdp)

                    if isfile(new_mdp):
                        mdp = new_mdp
                    else:
                        mdp = args.mdp

                    print("\nGenerating GROMACS TPR file using {} grompp\n".format(args.gmx))
                    import subprocess
                    subprocess.Popen("{} grompp -f {} -c {} -p {} -n {} -o {} -quiet".format(args.gmx, mdp,
                                                                                                            args.coords, args.top,
                                                                                                            args.ndx, args.tpr),
                                                                                                            stdout=subprocess.PIPE, shell=True).stdout.read()
                else:
                    print("MDP file not passed! Skipping generation of GROMACS TPR file")
        except (mimicpy.utils.errors.SelectionError, mimicpy.utils.errors.MiMiCPyError) as error:
            print(error)
            sys.exit()

    if args.sele:
        print("Reading selection from {}".format(args.sele))

        if not isfile(args.sele):
            print("Error: Cannot find file {}! Exiting..\n".format(args.sele))
            sys.exit(1)
        else:
            sele_txt =  mimicpy.utils.file_handler.read(args.sele)

        for line in sele_txt.splitlines():
            if check_user_input(line) == False:
                continue

        do_prep()
    else:
        print("\nPlease enter selection below. For more information type 'help'")

    while not args.sele:
        try:
            user_input = input('> ')
        except KeyboardInterrupt:
            print("Exiting without writing\n")
            sys.exit()

        if user_input.strip() in ['quit', 'q']:
            do_prep()
            break

        if check_user_input(user_input) == False:
            continue

def prepmm(mdp, qma, out):
    try:
        mimicpy.Preparation.get_gmx_input(mdp, qma, out)
    except FileNotFoundError as e:
        print('\n\nError: Cannot find file {}! Exiting..\n'.format(e.filename))
        sys.exit(1)
    except mimicpy.utils.errors.ScriptError as e:
        print(e)
        sys.exit(1)
        
def get_nsa_mpt(args, only_nsa=False):
    nsa_dct = {}
    if args.nsa:
        if args.top.split('.')[-1] == 'mpt':
            print("Non-standard atomtype file ignored as .mpt file was passed")
        else:
            print("\n**Reading non-standard atomtypes file**\n")

            if not isfile(args.nsa):
                print("Error: Cannot find file {}! Exiting..\n".format(args.nsa))
                sys.exit(1)
            else:
                nsa_txt =  mimicpy.utils.file_handler.read(args.nsa)
                for i, line in enumerate(nsa_txt.splitlines()):
                    splt = line.split()
                    if len(splt) < 2:
                        print("Line {} in {} is not in 2-column format!\n".format(i+1, args.nsa))
                        sys.exit(1)
                    elif len(splt) > 2:
                        print("Line {} in {} has more than 2-columns. \
                              Using first two values only.\n".format(i+1, args.nsa))

                    nsa_dct[splt[0]] = splt[1]

    if nsa_dct != {}:
        print("The following atomypes were read from {}:\n".format(args.nsa))
        mimicpy.utils.strings.print_dict(nsa_dct, "Atom Type", "Element", print)

    if only_nsa:
        return nsa_dct

    print("\n**Reading topology**\n")
    try:
        return mimicpy.Mpt.from_file(args.top, mode='r', nonstandard_atomtypes=nsa_dct,
                                buffer=args.buf, gmxdata=args.ff, guess_elements=args.guess)
    except FileNotFoundError as e:
        print('\n\nError: Cannot find file {}! Exiting..\n'.format(e.filename))
        sys.exit(1)
    except mimicpy.utils.errors.ParserError as e:
        print(e)
        sys.exit(1)

def getmpt(args):
    get_nsa_mpt(args).write(args.mpt)

def cpmdid(args):
    
    def print_idx(data, printer=print):
        if args.print == 'table':
            mimicpy.utils.strings.print_table(data, printer, new_line=(False if printer==print else True))
            return
        elif args.print == 'list':
            col_len = 5
            space_len = 6
            indices = data['CPMD ID']
            max_len = len(str(max(indices))) + 1
            spaces = space_len if max_len <= space_len else max_len
            txt = "No. of atoms: {}\n".format(len(indices))
            for i, idx in enumerate(indices):
                if i%col_len == 0:
                    txt += '\n'
                txt += "{:{}}".format(idx, spaces)
        elif args.print == 'range':
            indices = [int(i) for i in data['CPMD ID']]
            indices.sort()
            txt = ''
            for i, j in itertools.groupby(enumerate(indices), lambda x: x[1] - x[0]): 
                j = list(j)
                if j[0][0] != 0: txt += '\n'
                txt += "{} to {}".format(j[0][1], j[-1][1]) if j[0][1]!=j[-1][1] else str(j[0][1])
        printer(txt)
    
    args.print = args.print.lower()
    if args.print not in ['table', 'list', 'range']:
        print('\nError: Invalid option for -print. Only table, list, range accepted! Exiting..\n')
        sys.exit(1)
    
    try:
        cpmd = mimicpy.CpmdScript.from_file(args.inp)
        print("\n**Reading topology**\n")
        top = mimicpy.Top(args.top, buffer=args.buf, gmxdata=args.ff)
        id_conversion_dict = cpmd.gmx_to_cpmd_idx(top)
        mpt = mimicpy.Mpt.from_top(top)
    except mimicpy.utils.errors.ParserError as e:
        print(e)
        sys.exit(1)
    except FileNotFoundError as e:
        print('\n\nError: Cannot find file {}! Exiting..\n'.format(e.filename))
        sys.exit(1)
        
    readline.parse_and_bind('set editing-mode vi') # handle command history
    
    if args.sele:
        print("Reading selection from {}".format(args.sele))

        if not isfile(args.sele):
            print("Error: Cannot find file {}! Exiting..\n".format(args.sele))
            sys.exit(1)
        else:
            sele_txt =  mimicpy.utils.file_handler.read(args.sele)

    else:
        print("\nPlease enter selection below. Type q or quit to exit.")
    
        sele_txt = ''
    
        while not args.sele:
            try:
                user_input = input('> ')
            except KeyboardInterrupt:
                print("Exiting\n")
                sys.exit()
                
            user_input = user_input.strip()
            
            if user_input in ['quit', 'q']: break
            
            try:
                mpt.select(user_input)
            except (mimicpy.utils.errors.SelectionError, mimicpy.utils.errors.MiMiCPyError) as error:
                print(error)
                continue
                
            sele_txt += user_input + '\n'
    
    data = {'Atom': [], 'Residue': [], 'CPMD ID': []}
    
    for line in sele_txt.splitlines():
        try:
            sele = mpt.select(line)
        except (mimicpy.utils.errors.SelectionError, mimicpy.utils.errors.MiMiCPyError) as error:
            print(error)
            return None
            
        cpmd_idx = []
        for i in sele.index:
            cpmd_idx.append(str(id_conversion_dict[i]))
        
        mini_data = {'Atom': ["{} {}".format(a,b) for a,b in zip(sele.index, sele['name'])],\
                'Residue': ["{} {}".format(a,b) for a,b in zip(sele['resid'], sele['resname'])], 'CPMD ID': cpmd_idx}
        
        data['Atom'] += mini_data['Atom']
        data['Residue'] += mini_data['Residue']
        data['CPMD ID'] += mini_data['CPMD ID']
    
    if not args.out:
        print("\nThe CPMD IDs for the selected atoms are:\n")
        print_idx(data)
    elif args.out and data:
        print("\n**Writing table of selected atoms**\n")
        try:
            with open(args.out, 'w') as f:
                print_idx(data, f.write)
        except FileNotFoundError as e:
            print('\n\nError: Cannot find file {}! Exiting..\n'.format(e.filename))
            sys.exit(1)
        except FileExistsError as e:
            print(e)
            sys.exit(1)

def cpmd2coords(args):
    mpt = get_nsa_mpt(args)
    try:
        cpmd = mimicpy.CpmdScript.from_file(args.inp)
    except mimicpy.utils.errors.ParserError as e:
        print(e)
        sys.exit(1)
    except FileNotFoundError as e:
        print('\n\nError: Cannot find file {}! Exiting..\n'.format(e.filename))
        sys.exit(1)

    print('')
    loader = Loader('**Writing coordinates**')

    try:
        cpmd.to_coords(mpt, args.coords, title='Coordinates from {}'.format(args.inp))
    except mimicpy.utils.errors.MiMiCPyError as e:
        print(e)
        loader.close(halt=True)
        sys.exit(1)

    loader.close()
    
def geom2coords(args):
    try:
        cpmd = mimicpy.CpmdScript.from_file(args.inp)
        print("\n**Reading topology**\n")
        top = mimicpy.Top(args.top, buffer=args.buf, gmxdata=args.ff)
        id_conversion_dict = cpmd.gmx_to_cpmd_idx(top)
        mpt = mimicpy.Mpt.from_top(top)
    except mimicpy.utils.errors.ParserError as e:
        print(e)
        sys.exit(1)
    except FileNotFoundError as e:
        print('\n\nError: Cannot find file {}! Exiting..\n'.format(e.filename))
        sys.exit(1)
    
    ext = 'cpmd geo'
    if args.geom.split()[-1].lower() == 'xyz':
        ext = 'xyz'
    
    print('')
    loader = Loader('**Reading CPMD Geometry**')
    
    try:
        geom = mimicpy.CoordsIO(args.geom, ext=ext, cpmd=cpmd, top=top)
    except mimicpy.utils.errors.MiMiCPyError as e:
        print(e)
        loader.close(halt=True)
        sys.exit(1)
        
    loader.close()
    
    print('')
    loader = Loader('**Writing coordinates**')
    
    try:
        mimicpy.CoordsIO(args.coords, mode='w').write(mpt.select('all'), geom.coords)
    except mimicpy.utils.errors.MiMiCPyError as e:
        print(e)
        loader.close(halt=True)
        sys.exit(1)

    loader.close()

def fixtop(args):
    nsa_dct = get_nsa_mpt(args, True)
    print("\n**Reading topology**\n")
    try:
        top = mimicpy.Top(args.top, buffer=args.buf, nonstandard_atomtypes=nsa_dct,
                        gmxdata=args.ff, guess_elements=args.guess)
    except FileNotFoundError as e:
        print('\n\nError: Cannot find file {}! Exiting..\n'.format(e.filename))
        sys.exit(1)
    except mimicpy.utils.errors.ParserError as e:
        print(e)
        sys.exit(1)
    print("\n**Writing fixed [ atomtypes ] section**\n")

    try:
        top.write_atomtypes(args.out, delete_atomtypes=args.cls)
    except FileNotFoundError as e:
        print('\n\nError: Cannot find file {}! Exiting..\n'.format(e.filename))
        sys.exit(1)
    except FileExistsError as e:
        print(e)
        sys.exit(1)
    return top

def main():
    print('\n \t                ***** MiMiCPy *****                  ')
    print('\n \t                Running version {}\n\tFor more information type mimicpy [subcommand] --help \n'.format(mimicpy.__version__))

    parser = argparse.ArgumentParser(prog='mimicpy')
    subparsers = parser.add_subparsers(title='valid subcommands',
                                       metavar='')  # Turns off list of subcommands

    #####
    parser_prepqm = subparsers.add_parser('prepqm',
                                          help='create CPMD/MiMiC input and GROMACS index files')
    prepqm_input = parser_prepqm.add_argument_group('options to specify input')
    prepqm_input.add_argument('-top',
                              required=True,
                              help='topology file',
                              metavar='[.top]')
    prepqm_input.add_argument('-coords',
                              required=True,
                              help='coordinate file',
                              metavar='[.gro/.pdb]')
    prepqm_input.add_argument('-mdp',
                              required=False,
                              help='MDP script to generate GROMACS TPR file',
                              metavar='[.mdp]')
    prepqm_output = parser_prepqm.add_argument_group('options to specify output')
    prepqm_output.add_argument('-out',
                               default='cpmd.inp',
                               help='CPMD script for MiMiC run',
                               metavar='[.inp] (cpmd.inp)')
    prepqm_output.add_argument('-ndx',
                               default='index.ndx',
                               help='Gromacs index file',
                               metavar='[.ndx] (index.ndx)')
    prepqm_output.add_argument('-tpr',
                              required=False,
                              default='mimic.tpr',
                              help='Output GROMACS TPR filename',
                              metavar='[.tpr] (mimic.tpr)')
    prepqm_others = parser_prepqm.add_argument_group('other options')
    prepqm_others.add_argument('-guess',
                              required=False,
                              default=True,
                              type=__str2bool,
                              help='toggle guessing atomic elements',
                              metavar='(True)')
    prepqm_others.add_argument('-sele',
                              required=False,
                              help='file containing selection',
                              metavar='[.txt/.dat]')
    prepqm_others.add_argument('-pp',
                               required=False,
                               help='file containing pseudopotential information',
                               metavar='[.dat]')
    prepqm_others.add_argument('-bound',
                              required=False,
                              default=False,
                              type=__str2bool,
                              help='toggle guessing boundary atoms',
                              metavar='(False)')
    prepqm_others.add_argument('-nsa',
                              required=False,
                              help='file containing non-standard atomtypes in 2-column format',
                              metavar='[.txt/.dat]')
    prepqm_others.add_argument('-ff',
                              required=False,
                              help='path to force field data directory',
                              metavar='')
    prepqm_others.add_argument('-inp',
                              required=False,
                              help='CPMD template input script',
                              metavar='[.inp]')
    prepqm_others.add_argument('-gmx',
                              required=False,
                              default='gmx',
                              help='GROMACS executable to automatically generate TPR file',
                              metavar='(gmx)')
    prepqm_others.add_argument('-pad',
                              required=False,
                              type=__str2float,
                              default=0.0,
                              help='extra distance between qm atoms and wall in nm',
                              metavar='(0)')
    prepqm_others.add_argument('-abs',
                              required=False,
                              default=False,
                              type=__str2bool,
                              help='return QM cell size as absolute',
                              metavar='(False)')
    prepqm_others.add_argument('-qma',
                              required=False,
                              default='QMatoms',
                              help='name of QM atoms group in index file',
                              metavar='(QMatoms)')
    prepqm_others.add_argument('-path',
                              required=False,
                              help='path in the MIMIC section, overrides template',
                              metavar='')
    prepqm_others.add_argument('-q',
                              required=False,
                              type=__str2float,
                              help='charge of QM region, overrides default charge calculation',
                              metavar='')
    prepqm_others.add_argument('-buf',
                              required=False,
                              default=1000,
                              type=__str2int,
                              help='buffer size for reading input topology',
                              metavar='(1000)')
    prepqm_others.add_argument('-bufc',
                              required=False,
                              default=1000,
                              type=__str2int,
                              help='buffer size for reading input coordinates',
                              metavar='(1000)')
    parser_prepqm.set_defaults(func=prepqm)
    ##
    #####
    parser_cpmd2coords = subparsers.add_parser('cpmd2coords',
                                          help='convert CPMD/MiMiC input to coordinates')
    cpmd2coords_input = parser_cpmd2coords.add_argument_group('options to specify input')
    cpmd2coords_input.add_argument('-top',
                              required=True,
                              help='topology file',
                              metavar='[.top]')
    cpmd2coords_input.add_argument('-inp',
                              required=True,
                              help='CPMD input script with MIMIC/ATOMS sections',
                              metavar='[.inp]')
    cpmd2coords_output = parser_cpmd2coords.add_argument_group('options to specify output')
    cpmd2coords_output.add_argument('-coords',
                               default='mimic.gro',
                               help='coordinate file from CPMD/MIMIC script',
                               metavar='[.gro/.pdb] (mimic.gro)')
    cpmd2coords_others = parser_cpmd2coords.add_argument_group('other options')
    cpmd2coords_others.add_argument('-guess',
                              required=False,
                              type=__str2bool,
                              help='toggle guessing atomic elements',
                              metavar='(True)')
    cpmd2coords_others.add_argument('-nsa',
                              required=False,
                              help='file containing list of non-standard atomtypes',
                              metavar='[.txt/.dat]')
    cpmd2coords_others.add_argument('-ff',
                              required=False,
                              help='path to force field data directory',
                              metavar='')
    cpmd2coords_others.add_argument('-buf',
                              required=False,
                              default=1000,
                              type=__str2int,
                              help='buffer size for reading input topology',
                              metavar='(1000)')
    parser_cpmd2coords.set_defaults(func=cpmd2coords)
    ##
    #####
    parser_fixtop = subparsers.add_parser('fixtop',
                                          help='fix [ atomtypes ] section of GROMACS topology')
    fixtop_input = parser_fixtop.add_argument_group('options to specify input files')
    fixtop_input.add_argument('-top',
                              required=True,
                              help='GROMACS topology file',
                              metavar='[.top]')
    fixtop_output = parser_fixtop.add_argument_group('options to specify output files')
    fixtop_output.add_argument('-out',
                               default='atomtypes.itp',
                               help='fixed .itp file',
                               metavar='[.itp] (atomtypes.itp)')
    fixtop_others = parser_fixtop.add_argument_group('other options')
    fixtop_others.add_argument('-guess',
                              required=False,
                              default=True,
                              help='toggle guessing atomic elements',
                              metavar='(True)')
    fixtop_others.add_argument('-nsa',
                              required=False,
                              help='file containing list of non-standard atomtypes',
                              metavar='[.txt/.dat]')
    fixtop_others.add_argument('-ff',
                              required=False,
                              help='path to force field data directory',
                              metavar='')
    fixtop_others.add_argument('-cls',
                              required=False,
                              default=False,
                              type=__str2bool,
                              help='toggle clear [ atomtypes ] sections from files',
                              metavar='(False)')
    fixtop_others.add_argument('-buf',
                              required=False,
                              default=1000,
                              type=__str2int,
                              help='buffer size for reading input topology',
                              metavar='(1000)')
    parser_fixtop.set_defaults(func=fixtop)
    ##
    #####
    parser_cpmdid = subparsers.add_parser('cpmdid',
                                          help='find the CPMD ID of a selection of atoms')
    cpmdid_input = parser_cpmdid.add_argument_group('options to specify input files')
    cpmdid_input.add_argument('-top',
                              required=True,
                              help='GROMACS topology file',
                              metavar='[.top]')
    cpmdid_input.add_argument('-inp',
                              required=False,
                              help='CPMD input script',
                              metavar='[.inp]')
    cpmdid_others = parser_cpmdid.add_argument_group('options to specify output files')
    cpmdid_others.add_argument('-sele',
                              required=False,
                              help='file containing selection',
                              metavar='[.txt/.dat]')
    cpmdid_others.add_argument('-print',
                              required=False,
                              default='table',
                              help='print CPMD IDs as table, list or range',
                              metavar='table/list/range')
    cpmdid_others.add_argument('-ff',
                              required=False,
                              help='path to force field data directory',
                              metavar='')
    cpmdid_others.add_argument('-buf',
                              required=False,
                              default=1000,
                              type=__str2int,
                              help='buffer size for reading input topology',
                              metavar='(1000)')
    cpmdid_output = parser_cpmdid.add_argument_group('options to specify output')
    cpmdid_output.add_argument('-out',
                               required=False,
                               help='file to write to output containing CPMD IDs',
                               metavar='[.txt]')
    parser_cpmdid.set_defaults(func=cpmdid)
    ##
    #####
    parser_geom2coords = subparsers.add_parser('geom2coords',
                                          help='convert a CPMD GEOMETRY (or a GEOMETRY.xyz) file to coordinates')
    geom2coords_input = parser_geom2coords.add_argument_group('options to specify input files')
    geom2coords_input.add_argument('-geom',
                              required=False,
                              help='CPMD GEOMETRY',
                              metavar='[GEOMETRY/GEOMETRY.xyz]')
    geom2coords_input.add_argument('-top',
                              required=True,
                              help='GROMACS topology file',
                              metavar='[.top]')
    geom2coords_input.add_argument('-inp',
                              required=False,
                              help='CPMD input script',
                              metavar='[.inp]')
    geom2coords_others = parser_geom2coords.add_argument_group('options to specify output files')
    geom2coords_others.add_argument('-ff',
                              required=False,
                              help='path to force field data directory',
                              metavar='')
    geom2coords_others.add_argument('-buf',
                              required=False,
                              default=1000,
                              type=__str2int,
                              help='buffer size for reading input topology',
                              metavar='(1000)')
    geom2coords_output = parser_geom2coords.add_argument_group('options to specify output')
    geom2coords_output.add_argument('-coords',
                               default='GEOMETRY.gro',
                               help='coordinate file from CPMD GEOMETRY',
                               metavar='[.gro/.pdb] (GEOMETRY.gro)')
    parser_geom2coords.set_defaults(func=geom2coords)
    ##
    args = parser.parse_args()
    if vars(args) == {}:
        sys.exit()
    subcommand = args.func.__name__
    print('=====> Running {} <=====\n'.format(subcommand))
    args.func(args)
    print('\n=====> Done <=====\n')

if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        sys.exit(1)
