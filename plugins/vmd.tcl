#
#    MiMiCPy: Python Based Tools for MiMiC
#    Copyright (C) 2020-2023 Bharath Raghavan,
#                            Florian Schackert
#
#    This file is part of MiMiCPy.
#
#    MiMiCPy is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Lesser General Public License as
#    published by the Free Software Foundation, either version 3 of
#    the License, or (at your option) any later version.
#
#    MiMiCPy is distributed in the hope that it will be useful, but
#    WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Lesser General Public License for more details.
#
#    You should have received a copy of the GNU Lesser General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

proc prepqm {
        args
    } {

    array set options {
        -sele atomselect0
        -molid 0
        -sele_bound None
        -find_bound False
        -inp None
        -out cpmd.inp
        -ndx index.ndx
        -top topol.top
        -q 0
        -pad 0
        -pp None
        -abs False
        -path None
        -qma qmatoms
        }

    array set options $args

    set a [molinfo $options(-molid) get a]
    set b [molinfo $options(-molid) get b]
    set c [molinfo $options(-molid) get c]
    set alpha [molinfo $options(-molid) get alpha]
    set beta [molinfo $options(-molid) get beta]
    set gamma [molinfo $options(-molid) get gamma]

    set name [$options(-sele) get name]
    set index [$options(-sele) get index]
    set resname [$options(-sele) get resname]
    set x [$options(-sele) get x]
    set y [$options(-sele) get y]
    set z [$options(-sele) get z]

    if {$options(-sele_bound) eq "None"} {
        set name_ "None"
        set index_ 0
        set resname_ "None"
        set x_ 0
        set y_ 0
        set z_ 0
    } else {
        set name_ [$options(-sele_bound) get name]
        set index_ [$options(-sele_bound) get index]
        set resname_ [$options(-sele_bound) get resname]
        set x_ [$options(-sele_bound) get x]
        set y_ [$options(-sele_bound) get y]
        set z_ [$options(-sele_bound) get z]
    }

    puts $[exec mimicpy_vmd $options(-top) $options(-inp) $options(-ndx) $options(-out) $options(-molid) $options(-sele_bound) $options(-pad) $options(-abs) $options(-qma) $options(-path) $options(-q) $options(-pp)\
        $options(-find_bound) $name $index $resname $x $y $z $name_ $index_ $resname_ $x_ $y_ $z_\
        $a $b $c $alpha $beta $gamma]
}
