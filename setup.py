#!/usr/bin/env python

import shutil
from setuptools import setup, find_packages
from distutils.dir_util import copy_tree

def get_package():
    root = 'mimicpy'
    return  [root]+[root+'.'+i for i in find_packages(root)]

with open("README.md", "r") as f:
    long_description = f.read()

copy_tree('plugins', 'mimicpy/plugins') # copy plugins dir from root to mimicpy

setup(
    name='mimicpy',
    version='0.2.1',
    zip_safe=True,
    description='Companion library to the MiMiC framework for input preparation.',
    author="Bharath Raghavan, Florian K. Schackert",
    author_email='b.raghavan@fz-juelich.de',
    long_description=long_description,
    long_description_content_type="text/markdown",
    license="GNU Lesser General Public License v3 or later (LGPLv3+)",
    platforms="OS Independent",
    url="https://gitlab.com/MiMiC-projects/mimicpy",
    project_urls = {
      'Issues': 'https://gitlab.com/MiMiC-projects/mimicpy/issues',
      'User Support': 'https://gitlab.com/MiMiC-projects/user-support',
      'Documentation': 'https://mimic-project.org/'
    },
    include_package_data=True,
    package_data={
            "mimicpy": ["plugins/*.py", "plugins/vmd.tcl"],
        },
    packages=get_package(),
    install_requires=['numpy>=1.12.0', 'pandas>=0.24.0'],
    python_requires='>=3.5',
    classifiers=[
            "Programming Language :: Python :: 3",
            "License :: OSI Approved :: GNU Lesser General Public License v3 or later (LGPLv3+)",
            "Operating System :: OS Independent",
            "Topic :: Scientific/Engineering :: Physics"
        ],
    entry_points = {
        'console_scripts': [
            'mimicpy = mimicpy.__main__:main',
            'mimicpy_vmd = mimicpy.plugins.__main_vmd__:main',
            'mimicpy_plugin_installer = mimicpy.plugins.__main_installer__:main'
        ],
    }
)

shutil.rmtree('mimicpy/plugins') # remove the plugin dir in mimicpy